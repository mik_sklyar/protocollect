
#import "ViewController.h"
#import "VerticalListLayout.h"
#import "PhotoCell.h"
#import "DataSource.h"
#import "ImageLoader.h"

const CGFloat margin = 10.f;
const CGFloat fakeCellHeight = 0.f;

@interface ViewController () <
UICollectionViewDataSource,
UICollectionViewDelegate,
UICollectionViewDelegateVerticalListLayout
>

@property (nonatomic, weak) UICollectionView *collectionView;
@property (nonatomic, weak) UIRefreshControl *refreshControl;

@property (nonatomic, strong, readonly) DataSource *dataSource;
@property (nonatomic, strong, readonly) ImageLoader *imageLoader;

@end

@implementation ViewController

- (instancetype)init {
    if (self = [super init]) {
        _dataSource = [[DataSource alloc] init];
        _imageLoader = [[ImageLoader alloc] initWithFolderName:@"BearsPhotoCache"];
        [self reloadData];
        
        self.navigationItem.title = NSLocalizedString(@"Bears", @"mainVC.title");
        self.navigationItem.rightBarButtonItem =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:
         UIBarButtonSystemItemRefresh target:self action:@selector(reloadData)];
    }
    return self;
}

#pragma mark - View lifecycle

- (void)loadView {
    self.view = [[UIView alloc] init];
    
    VerticalListLayout *layout = [[VerticalListLayout alloc] init];
    layout.itemMargin = margin;
    layout.contentInsets = (UIEdgeInsets){margin, margin, 0.f, margin};
    layout.sectionInsets = (UIEdgeInsets){0.f, 0.f, margin, 0.f};
    layout.squareItems = YES;
    
    UICollectionView *const cv = [[UICollectionView alloc]
                                  initWithFrame:self.view.bounds
                                  collectionViewLayout:layout];
    cv.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    cv.backgroundColor = [UIColor whiteColor];
    cv.alwaysBounceVertical = YES;
    cv.opaque = YES;
    
    cv.dataSource = self;
    cv.delegate = self;
    [cv registerClass:[PhotoCell class] forCellWithReuseIdentifier:@"riItemPhoto"];
    [cv registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"riItemFake"];
    
    UIRefreshControl *rc = [[UIRefreshControl alloc] init];
    [rc addTarget:self action:@selector(reloadData)
             forControlEvents:UIControlEventValueChanged];

    [cv addSubview:rc];
    [self.view addSubview:cv];
    self.collectionView = cv;
    self.refreshControl = rc;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self patchCollectionViewContentInset:YES];
    [self refreshCollectionView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self patchCollectionViewContentInset:NO];
}

#pragma mark - Auxiliaries

- (void)reloadData {
    [self.imageLoader clearCache];
    
    [self.dataSource loadDataWithCompletion:^(NSError *error) {
        if (error) {
            //TODO: show alert
            return;
        }
        [self.refreshControl endRefreshing];
        [self refreshCollectionView];
    }];
}

- (void)refreshCollectionView {
    [self.collectionView reloadData];
}

- (void)removeDataForIndexPath:(NSIndexPath *)indexPath {
    [self.dataSource removeItemAtIndex:indexPath.row];
}

- (void)collectionView:(UICollectionView *)collectionView
 removeItemAtIndexPath:(NSIndexPath *)indexPath {

    UICollectionViewCell *const cell = [collectionView cellForItemAtIndexPath:indexPath];
    const CGRect originalFrame = cell.frame;
    CGRect hiddenFrame = originalFrame;
    hiddenFrame.origin.x = collectionView.bounds.size.width;
    
    collectionView.userInteractionEnabled = NO;
    [UIView animateWithDuration:[CATransaction animationDuration] animations:^{
        cell.frame = hiddenFrame;
        

    } completion:^(BOOL finished){
        cell.hidden = YES;
        cell.frame = originalFrame;
        
        if (self.dataSource.count == 2) {
            //fix of weird animation interruption
            CGPoint offset = (CGPoint){
                self.collectionView.contentOffset.x,
                -self.collectionView.contentInset.top
            };
            [self.collectionView setContentOffset:offset animated:YES];
        }
        
        [collectionView performBatchUpdates:^{
            [self removeDataForIndexPath:indexPath];
            [collectionView deleteItemsAtIndexPaths:@[indexPath]];
        } completion:^(BOOL finished) {
            cell.hidden = NO;
            collectionView.userInteractionEnabled = YES;
        }];
    }];
}

#pragma mark - Patch of UICollectionView behavior

- (void)patchCollectionViewContentInset:(BOOL)patch {
    CGFloat factor = (patch) ? -1.f : 0.f;
    UIEdgeInsets insets = self.collectionView.contentInset;
    insets.bottom = factor * (fakeCellHeight + margin);
    self.collectionView.contentInset = insets;
}

- (BOOL)isFakeIndex:(NSInteger)index {
    return (index == self.dataSource.count);
}

#pragma mark - UICollectionViewDelegate & UICollectionViewDataSource

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isFakeIndex:indexPath.row]) return;
    
    Entity *entity = [self.dataSource itemAtIndex:indexPath.row];
    if ([self.imageLoader hasImageForToken:entity.identifier]) {
        //remove only loaded cells
        [self collectionView:collectionView removeItemAtIndexPath:indexPath];
    } else {
        //TODO: something else
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return self.dataSource.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isFakeIndex:indexPath.row]) {
        return [collectionView dequeueReusableCellWithReuseIdentifier:
                @"riItemFake" forIndexPath:indexPath];
    }
    
    PhotoCell *cell =
    [collectionView dequeueReusableCellWithReuseIdentifier:
     @"riItemPhoto" forIndexPath:indexPath];

    Entity *entity = [self.dataSource itemAtIndex:indexPath.row];
    cell.titleLabel.text = NSLocalizedString(entity.title, nil);
    
    [self.imageLoader loadImageWithToken:entity.identifier url:entity.url completion:
     [cell processBlokWithToken:entity.identifier]];
    
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(VerticalListLayout *)layout
 heightForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isFakeIndex:indexPath.row]) {
        return fakeCellHeight;
    }
    return VerticalListLayoutDelegateDontCare;
}

@end
