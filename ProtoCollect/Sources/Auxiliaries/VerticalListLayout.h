
@import UIKit;

@interface VerticalListLayout : UICollectionViewLayout

@property (nonatomic, assign) UIEdgeInsets contentInsets;
@property (nonatomic, assign) UIEdgeInsets sectionInsets;
@property (nonatomic, assign) CGFloat itemMargin;
@property (nonatomic, assign) CGFloat itemHeight;
@property (nonatomic, assign) BOOL squareItems; //if YES cellHeight & delegate method are ignored

@end

@protocol UICollectionViewDelegateVerticalListLayout <UICollectionViewDelegate>
@optional

//return in following methods to disclaim responsibility
extern const CGFloat VerticalListLayoutDelegateDontCare;

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(VerticalListLayout *)layout
 heightForItemAtIndexPath:(NSIndexPath *)indexPath;

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(VerticalListLayout *)layout
 heightForHeaderInSection:(NSInteger)section;

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(VerticalListLayout *)layout
 heightForFooterInSection:(NSInteger)section;

@end