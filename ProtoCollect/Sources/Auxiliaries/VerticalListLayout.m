
#import "VerticalListLayout.h"

const CGFloat VerticalListLayoutDelegateDontCare = -1.f;

@interface VerticalListLayout ()

@property (nonatomic, strong) NSDictionary *layoutAttributes;
@property (nonatomic, assign) CGFloat itemWidth;
@property (nonatomic, assign) CGSize contentSize;

@end

@implementation VerticalListLayout

#pragma mark - Overrides

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return !CGSizeEqualToSize(self.collectionView.bounds.size, newBounds.size);
}

- (CGSize)collectionViewContentSize {
    return _contentSize;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryViewOfKind:(NSString *)elementKind
                                                                     atIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [self layoutKeyForSuppIndexPath:indexPath];
    return _layoutAttributes[key];
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    return _layoutAttributes[[self layoutKeyForIndexPath:indexPath]];
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSPredicate *const predicate =
    [NSPredicate predicateWithBlock:^BOOL(NSString *evaluatedObject, NSDictionary *bindings) {
        return CGRectIntersectsRect(rect, [_layoutAttributes[evaluatedObject] frame]);
    }];
    NSArray *const matchingKeys =
    [[_layoutAttributes allKeys] filteredArrayUsingPredicate:predicate];
    return [_layoutAttributes objectsForKeys:matchingKeys notFoundMarker:[NSNull null]];
}

- (void)prepareLayout {
    [super prepareLayout];
    
    const CGFloat hOffset = self.contentInsets.left + self.sectionInsets.left;
    CGFloat vOffset = self.contentInsets.top;
    
    id<UICollectionViewDelegateVerticalListLayout> const delegate = (id<UICollectionViewDelegateVerticalListLayout>)self.collectionView.delegate;
    const BOOL canAskDelegateAboutHeader = [delegate respondsToSelector:@selector(collectionView:layout:heightForHeaderInSection:)];
    const BOOL canAskDelegateAboutFooter = [delegate respondsToSelector:@selector(collectionView:layout:heightForFooterInSection:)];
    
    self.itemWidth = self.collectionView.bounds.size.width -
    (hOffset + self.sectionInsets.right + self.contentInsets.right);
    NSMutableDictionary *const layoutAttributes = [NSMutableDictionary dictionary];
    
    const NSUInteger numberOfSections = [self.collectionView numberOfSections];
    for (NSInteger section = 0; section < numberOfSections; section++) {

        // Header
        if (canAskDelegateAboutHeader) {
            const CGFloat height =
            MAX(0.f, [delegate collectionView:self.collectionView layout:
                      self heightForHeaderInSection:section]);
            const CGRect frame = (CGRect){hOffset, vOffset, self.itemWidth, height};

            UICollectionViewLayoutAttributes *const attributes =
            [self attributesForSuppInSection:section frame:frame isFooter:NO];
            [layoutAttributes setObject:attributes forKey:
             [self layoutKeyForSuppIndexPath:attributes.indexPath]];

            vOffset += height;
        }
        
        // Items
        NSUInteger const numberOfItems = [self.collectionView numberOfItemsInSection:section];
        for (NSInteger item = 0; item < numberOfItems; item++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:section];
            NSString *key = [self layoutKeyForIndexPath:indexPath];
            
            vOffset += (item == 0) ? self.sectionInsets.top : self.itemMargin;
            CGFloat itemHeight = [self itemHeighForIndexPath:indexPath];

            UICollectionViewLayoutAttributes *attributes =
            [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
            attributes.frame = (CGRect){hOffset, vOffset, self.itemWidth, itemHeight};
            layoutAttributes[key] = attributes;
            
            vOffset += itemHeight;
            if (item == numberOfItems - 1) vOffset += self.sectionInsets.bottom;
        }
        
        // Footer
        if (canAskDelegateAboutFooter) {
            const CGFloat height =
            MAX(0.f, [delegate collectionView:self.collectionView layout:
                      self heightForFooterInSection:section]);
            const CGRect frame = (CGRect){hOffset, vOffset, self.itemWidth, height};
            
            UICollectionViewLayoutAttributes *const attributes =
            [self attributesForSuppInSection:section frame:frame isFooter:YES];
            [layoutAttributes setObject:attributes forKey:
             [self layoutKeyForSuppIndexPath:attributes.indexPath]];

            vOffset += height;
        }
    }
    self.contentSize = (CGSize){
        self.collectionView.bounds.size.width,
        vOffset + self.contentInsets.bottom
    };
    self.layoutAttributes = [layoutAttributes copy];
}

#pragma mark - Auxiliaries

- (CGFloat)itemHeighForIndexPath:(NSIndexPath *)indexPath {
    CGFloat itemHeight = VerticalListLayoutDelegateDontCare;
    
    BOOL shouldAskDelegate = [self.collectionView.delegate respondsToSelector:
                              @selector(collectionView:layout:heightForItemAtIndexPath:)];
    if (shouldAskDelegate) {
        itemHeight =
        [(id<UICollectionViewDelegateVerticalListLayout>)self.collectionView.delegate
         collectionView:self.collectionView layout:self heightForItemAtIndexPath:indexPath];
    }
    if (itemHeight == VerticalListLayoutDelegateDontCare) {
        itemHeight = (self.squareItems) ? self.itemWidth : self.itemHeight;
    }
    return itemHeight;
}

- (NSString *)layoutKeyForIndexPath:(NSIndexPath *)indexPath {
    return [NSString stringWithFormat:@"%ld_%ld", (long)indexPath.section, (long)indexPath.row];
}

- (NSString *)layoutKeyForSuppIndexPath:(NSIndexPath *)indexPath {
    return [NSString stringWithFormat:@"s_%ld_%ld", (long)indexPath.section, (long)indexPath.row];
}

- (UICollectionViewLayoutAttributes *)attributesForSuppInSection:(NSInteger)section
                                                        frame:(CGRect)frame
                                                     isFooter:(BOOL)footer {
    NSIndexPath *const indexPath =
    [NSIndexPath indexPathForItem:(NSInteger)footer inSection:section];
    NSString *const kind = (footer) ?
    UICollectionElementKindSectionFooter : UICollectionElementKindSectionHeader;
    UICollectionViewLayoutAttributes *const attributes =
    [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:kind withIndexPath:indexPath];
    attributes.frame = frame;
    return attributes;
}

@end
