
@import UIKit;

#import "ImageReceiver.h"

@interface PhotoCell : UICollectionViewCell <ImageReceiver>

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@end
