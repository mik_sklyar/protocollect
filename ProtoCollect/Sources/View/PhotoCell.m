
#import "PhotoCell.h"

@interface PhotoCell ()

@property (nonatomic, strong) NSString *token;

@end


@implementation PhotoCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (!(self = [super initWithFrame:frame])) return nil;
    
    UILabel *label = [[UILabel alloc] initWithFrame:self.bounds];
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 0;
    [self.contentView addSubview:label];
    self.titleLabel = label;
    
    UIImageView *iv = [[UIImageView alloc] initWithFrame:self.bounds];
    iv.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    iv.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:iv];
    self.imageView = iv;
    
    CALayer *layer = self.layer;
    layer.cornerRadius = 10.f;
    layer.borderWidth = 1.f;
    layer.borderColor = [UIColor grayColor].CGColor;
    layer.shouldRasterize = YES;
    layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    self.clipsToBounds = YES;
    
    return self;
}

- (void)prepareForReuse {
    self.token = nil;
    self.imageView.image = nil;
    self.titleLabel.text = nil;
}

- (ImageReceiverBlock)processBlokWithToken:(NSString *)token {
    self.token = token;
    __weak typeof(self) weakSelf = self;
    ImageReceiverBlock block = ^(NSString *token, UIImage *image, NSError *error) {
        if ([weakSelf.token isEqualToString:token]) {
            weakSelf.imageView.image = image;
        }
    };
    return block;
}

@end
