
#import "AppHelper.h"
#import "ViewController.h"

@interface AppHelper ()

@end

@implementation AppHelper

+ (UIViewController *)rootViewController {
 
    static dispatch_once_t onceToken;
    static UIViewController *vc = nil;
    dispatch_once(&onceToken, ^{
        vc = [[ViewController alloc] init];
        vc = [[UINavigationController alloc] initWithRootViewController:vc];
    });
    return vc;
}

@end
