
@import UIKit;
#import "ImageReceiver.h"

@interface ImageLoader : NSObject

- (instancetype)init __attribute__((unavailable("please use 'initWithFolderName:' method instead")));
- (instancetype)initWithFolderName:(NSString *)folderName;

- (void)clearCache;

- (BOOL)hasImageForToken:(NSString *)token;

- (void)loadImageWithToken:(NSString *)token
                       url:(NSString *)url
                completion:(ImageReceiverBlock)completion;

@end
