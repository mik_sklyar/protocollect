
#import "DataSource.h"

@interface DataSource ()

@property (nonatomic, assign) BOOL isBusy;
@property (nonatomic, strong) NSMutableArray *items;

@end

@implementation DataSource

- (NSUInteger)count {
    return self.items.count;
}

- (void)setItems:(NSMutableArray *)items {
    [self willChangeValueForKey:@"count"];
    _items = items;
    [self didChangeValueForKey:@"count"];
}

- (void)removeItemAtIndex:(NSInteger)index {
    if (index >= self.count) return;
    
    [self willChangeValueForKey:@"count"];
    [self.items removeObjectAtIndex:index];
    [self didChangeValueForKey:@"count"];
}

- (void)addItem:(Entity *)item {
    if (!item) return;
    
    [self willChangeValueForKey:@"count"];
    [self.items addObject:item];
    [self didChangeValueForKey:@"count"];
}

- (Entity *)itemAtIndex:(NSInteger)index {
    if (index >= self.count) return nil;
    
    return [self.items objectAtIndex:index];
}


- (void)loadDataWithCompletion:(DataSourceCompletion)block {
    self.isBusy = YES;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSError *error;
        NSString *const path = [[NSBundle mainBundle] pathForResource:@"Data" ofType:@"plist"];
        NSArray *array = [NSArray arrayWithContentsOfFile:path];
        NSMutableArray *items = [NSMutableArray array];
        [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            Entity *entity = [[Entity alloc] initWithDictionary:obj];
            if (entity) {
                [items addObject:entity];
            } else {
                //TODO: make error
            }
        }];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.items = items;
            if (block) {
                block(error);
            }
            self.isBusy = NO;
        });
    });
}

@end
