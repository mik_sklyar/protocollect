
@import UIKit;

typedef void (^ImageReceiverBlock)(NSString *token, UIImage *image, NSError *error);

@protocol ImageReceiver <NSObject>

- (ImageReceiverBlock)processBlokWithToken:(NSString *)token;

@end
