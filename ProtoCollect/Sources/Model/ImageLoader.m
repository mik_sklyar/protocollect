
#import "ImageLoader.h"
#import "Reachability.h"

@interface ImageLoader ()

@property (nonatomic, strong, readonly) NSCache *memoryCache;
@property (nonatomic, strong, readonly) NSString *diskCachePath;
@property (nonatomic, strong, readonly) NSMapTable *tokensBlocks;
@property (nonatomic, strong, readonly) NSHashTable *loadedTokens;

@end

#define fileManager [NSFileManager defaultManager]
#define Error(__domain__, __code__) [NSError errorWithDomain:__domain__ code:__code__ userInfo:nil]

@implementation ImageLoader

- (instancetype)initWithFolderName:(NSString *)folderName {
    if (!folderName.length) return nil;
    if (self = [super init]) {
        @try {
            NSString *const caches =
            [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)
             objectAtIndex:0];
            NSString *const path = [caches stringByAppendingPathComponent:folderName];

            if (![fileManager fileExistsAtPath:path]) {
                NSError *error = nil;
                [fileManager createDirectoryAtPath:path withIntermediateDirectories:YES
                                        attributes:nil error:&error];
                if (error) return nil;
            }
            _diskCachePath = path;
        }
        @catch (NSException *exception) {
            return nil;
        }
        @finally {}

        _memoryCache = [[NSCache alloc] init];
        _tokensBlocks = [[NSMapTable alloc] init];
        _loadedTokens = [[NSHashTable alloc] init];
    }
    return self;
}

- (void)clearCache {
    [self.memoryCache removeAllObjects];
    [self.loadedTokens removeAllObjects];

    NSError *error = nil;
    NSArray *const files =
    [fileManager contentsOfDirectoryAtPath:self.diskCachePath error:&error];
    for (NSString *file in files) {
        [fileManager removeItemAtPath:
         [self.diskCachePath stringByAppendingPathComponent:file] error:&error];
    }
}

- (void)copyFileFrom:(NSString *)tmpPath to:(NSString *)filePath {
    NSError *error = nil;
    if ([fileManager fileExistsAtPath:filePath]) {
        [fileManager removeItemAtPath:filePath error:&error];
    }
    [fileManager copyItemAtPath:tmpPath toPath:filePath error:&error];
}

- (BOOL)hasImageForToken:(NSString *)token {
    return [self.loadedTokens containsObject:token];
}

- (void)loadImageWithToken:(NSString *)token
                       url:(NSString *)urlStr
                completion:(ImageReceiverBlock)completion {
    if (!token.length) return;

    UIImage *const cachedImage = [self.memoryCache objectForKey:token];
    if (cachedImage) {
        if (completion) completion(token, cachedImage, nil);
        return;
    }
    
    const BOOL alreadyInProgress = [self.tokensBlocks objectForKey:token] != nil;
    if (alreadyInProgress) {
        if (completion) {
            //just replace completion block for working operation
            [self.tokensBlocks setObject:completion forKey:token];
        }
        return;
    }
    
    [self.tokensBlocks setObject:(id)completion ?: [NSNull null] forKey:token];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *const filePath = [self.diskCachePath stringByAppendingPathComponent:token];
        UIImage *const image = [UIImage imageWithContentsOfFile:filePath];
        NSError *error = nil;
        if (!image) {
            NSURL *const url = [NSURL URLWithString:urlStr];
            if (!url || !url.scheme || !url.host) {
                //TODO: addition validation of URL
                error = Error(NSURLErrorDomain, NSURLErrorBadURL);
            } else if (![Reachability isInternetReachable]) {
                error = Error(NSURLErrorDomain, NSURLErrorNetworkConnectionLost);
            }
        }
        if (image || error) {
            [self finishLoadingWithToken:token mage:image error:error];
            return;
        }

        NSURLSessionDownloadTask *const task =
        [[NSURLSession sharedSession] downloadTaskWithURL:
         [NSURL URLWithString:urlStr] completionHandler:
         ^(NSURL *location, NSURLResponse *response, NSError *error) {
             UIImage *image = nil;
             if (!error) {
                 [self copyFileFrom:location.path to:filePath];
                 image = [UIImage imageWithContentsOfFile:filePath];
             }
             [self finishLoadingWithToken:token mage:image error:error];
         }];
        [task resume];
    });
}

- (void)finishLoadingWithToken:(NSString *)token mage:(UIImage *)image
                         error:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        ImageReceiverBlock completion = [self.tokensBlocks objectForKey:token];
        if (image) {
            [self.memoryCache setObject:image forKey:token];
            [self.loadedTokens addObject:token];
        }
        if (completion && ((id)completion != [NSNull null])) {
            [self.tokensBlocks removeObjectForKey:token];
            completion(token, image, error);
        }
    });
}

@end
