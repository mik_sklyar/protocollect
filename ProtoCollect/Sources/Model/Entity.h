
@import Foundation;

@interface Entity : NSObject

@property (nonatomic, strong, readonly) NSString *identifier;
@property (nonatomic, strong, readonly) NSString *title;
@property (nonatomic, strong, readonly) NSString *url;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
