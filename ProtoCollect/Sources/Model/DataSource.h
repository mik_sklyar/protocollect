
@import Foundation;

#import "Entity.h"

typedef void (^DataSourceCompletion)(NSError *error);

@interface DataSource : NSObject

@property (nonatomic, assign, readonly) BOOL isBusy;
@property (nonatomic, assign, readonly) NSUInteger count;

- (Entity *)itemAtIndex:(NSInteger)index;
- (void)removeItemAtIndex:(NSInteger)index;

- (void)loadDataWithCompletion:(DataSourceCompletion)block;

@end
