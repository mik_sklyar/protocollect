
#import "Entity.h"

@interface Entity ()

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *url;

@end

@implementation Entity

- (instancetype)init {
    return [self initWithDictionary:nil];
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (dictionary && ![dictionary isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    if (!(self = [super init])) return nil;

    [dictionary enumerateKeysAndObjectsUsingBlock:^(NSString *key, id obj, BOOL *stop) {
        if ([key isKindOfClass:[NSString class]]) {
            [self setValue:obj forKey:key];
        }
    }];
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"Entity {id:%@, title:\"%@\" ,url:%@}",
            self.identifier, self.title, self.url];
}

@end
